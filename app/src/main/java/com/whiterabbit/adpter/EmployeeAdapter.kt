package com.whiterabbit.adpter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidpos.common.CircleTransform
import com.whiterabbit.EmployeeActivity
import com.whiterabbit.R

import com.squareup.picasso.Picasso
import com.whiterabbit.EmployeDetailsActivity
import com.whiterabbit.models.EmployeeResponse


class EmployeeAdapter(
    employeeResponse: MutableList<EmployeeResponse>,
    mContext: EmployeeActivity
) :
    RecyclerView.Adapter<EmployeeAdapter.ViewHolder>() {
    var employeeResponse = employeeResponse;
    val mContext = mContext
    private val picasso = Picasso.get()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(employee: EmployeeResponse) {
            val profileImage = itemView.findViewById<ImageView>(R.id.iv_employee_image)
            val container = itemView.findViewById<RelativeLayout>(R.id.rl_continer)
            val name = itemView.findViewById<TextView>(R.id.tv_employee_name)
            val company = itemView.findViewById<TextView>(R.id.tv_company_name)
            picasso.load(employee.profileImage).transform(CircleTransform()).into(profileImage)
            name.text=employee.name
            if (employee.company!=null)
            company.text= employee.company!!.name

            container.setOnClickListener {
                val intent = Intent(mContext, EmployeDetailsActivity::class.java)
                intent.putExtra("employeeid",employee.id)
                mContext.startActivity(intent)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.employee_payload, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(employeeResponse.get(position))
    }

    override fun getItemCount(): Int {
        return employeeResponse!!.size;
    }

    fun refreshAdapter(employeeResponse: MutableList<EmployeeResponse>) {
        this.employeeResponse=employeeResponse
        notifyDataSetChanged()
    }


}