package com.whiterabbit.models


import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.google.gson.reflect.TypeToken


@Entity(tableName = "employee_table")
class EmployeeResponse {
    @SerializedName("id")
    @Expose
    @PrimaryKey(autoGenerate = true)
    var id: Int? = 0

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("username")
    @Expose
    var username: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("profile_image")
    @Expose
    var profileImage: String? = null

    @SerializedName("address")
    @Expose
    var address: Address? = Address()

    @SerializedName("phone")
    @Expose
    var phone: String? = null

    @SerializedName("website")
    @Expose
    var website: String? = null

    @SerializedName("company")
    @Expose
    var company: Company? = Company()
}

class AddressConverter {
    var gson = Gson()

    @TypeConverter
    fun ToJson(address: Address): String {
        return gson.toJson(address)
    }

    @TypeConverter
    fun ToObject(data: String): Address? {
        val listType = object : TypeToken<Address>() {
        }.type
        return gson.fromJson(data, listType)
    }
}

class CompanyConverter {
    var gson = Gson()

    @TypeConverter
    fun ToJson(company: Company?): String {
        return gson.toJson(company)
    }

    @TypeConverter
    fun ToObject(data: String?): Company? {


        val  listType = object : TypeToken<Company>() {}.type
        return gson.fromJson(data, listType)
    }
}

