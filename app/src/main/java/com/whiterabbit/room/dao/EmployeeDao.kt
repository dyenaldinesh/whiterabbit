package com.whiterabbit.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.whiterabbit.models.EmployeeResponse



@Dao
interface EmployeeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(products: MutableList<EmployeeResponse>?)

    @Query("SELECT * FROM employee_table")
    fun getAllEmployess():MutableList<EmployeeResponse>

    @Query("DELETE FROM employee_table")
    fun deleteEmployess()

    @Query("SELECT * FROM employee_table WHERE id=:arg0")
    fun getEmployee(arg0: String): EmployeeResponse
}