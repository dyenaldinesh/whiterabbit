package com.whiterabbit.room

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import com.whiterabbit.models.AddressConverter
import com.whiterabbit.models.CompanyConverter
import com.whiterabbit.models.EmployeeResponse
import com.whiterabbit.room.dao.EmployeeDao


@Database(entities = arrayOf(EmployeeResponse::class), version = 2)
@TypeConverters(AddressConverter::class,CompanyConverter::class)

abstract class AppDatabase : RoomDatabase() {

    abstract fun EmployeeDao(): EmployeeDao


    companion object {
        private var instance: AppDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context): AppDatabase {
            if(instance == null)
                instance = Room.databaseBuilder(ctx.applicationContext, AppDatabase::class.java,
                    "note_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build()

            return instance!!

        }

        private val roomCallback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
               // populateDatabase(instance!!)
            }
        }


    }



}