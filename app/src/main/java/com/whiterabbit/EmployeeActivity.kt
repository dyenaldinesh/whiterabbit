package com.whiterabbit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.whiterabbit.adpter.EmployeeAdapter
import com.whiterabbit.room.AppDatabase
import com.example.androidpos.service.ApiService
import com.example.androidpos.service.NetworkModule
import com.whiterabbit.models.EmployeeResponse
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EmployeeActivity : AppCompatActivity() {
    private lateinit var employeeAdapter: EmployeeAdapter
    private var instance: AppDatabase? = null
    lateinit var employeeResponse: MutableList<EmployeeResponse>
    private var networkRepository: ApiService = NetworkModule.createService(ApiService::class.java)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        initRecyclerView()
        getFromDb()
    }

    private fun initView() {
        supportActionBar?.hide()
        instance = AppDatabase.getInstance(this);
        employeeResponse = ArrayList()
        employeeAdapter = EmployeeAdapter(employeeResponse, this)
        sv_search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Log.d("searchvalue", newText.toString())
                var searchList: MutableList<EmployeeResponse>? = ArrayList()
                for (employess in employeeResponse!!.iterator()) {
                    if (employess.name!!.lowercase().contains(newText.toString().lowercase()) ||
                        employess.email!!.lowercase().contains(newText.toString().lowercase())
                    ) {
                        searchList!!.addAll(listOf(employess))
                    }
                }
                if (searchList != null) {
                    employeeAdapter.refreshAdapter(searchList)
                }

                return false
            }
        })


    }

    private fun getFromDb() {
        GlobalScope.launch() {
            try {
                employeeResponse= instance?.EmployeeDao()?.getAllEmployess()!!
                if (employeeResponse.size>0){
                    employeeAdapter.refreshAdapter(employeeResponse)
                }else{
                    getEmployees()
                }

                }catch (e: Exception) {

                }
            }

    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        rv_employee.layoutManager = layoutManager
        rv_employee.adapter = employeeAdapter
    }

    private fun getEmployees() {
        networkRepository.getEmployees().enqueue(object : Callback<MutableList<EmployeeResponse>> {
            override fun onResponse(
                call: Call<MutableList<EmployeeResponse>>,
                response: Response<MutableList<EmployeeResponse>>
            ) {
                if (response.isSuccessful) {
                    employeeResponse = response.body()!!
                    employeeAdapter.refreshAdapter(employeeResponse)
                    GlobalScope.launch {
                        instance!!.EmployeeDao().deleteEmployess()
                        instance!!.EmployeeDao().insert(employeeResponse)
                    }

                } }
            override fun onFailure(call: Call<MutableList<EmployeeResponse>>, t: Throwable) {

            }

        })
    }


}


