package com.example.androidpos.service

import com.example.androidpos.common.Configs
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder

import com.google.gson.Gson




object NetworkModule {
    var gson = GsonBuilder()
        .setLenient()
        .create()


    private val retrofit = Retrofit.Builder()
        .baseUrl(Configs.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    fun <S> createService(serviceClass: Class<S>?): S {
        return retrofit.create(serviceClass)
    }
}