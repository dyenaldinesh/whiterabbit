package com.example.androidpos.service

import com.whiterabbit.models.EmployeeResponse


import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @GET("5d565297300000680030a986")
    fun getEmployees():Call<MutableList<EmployeeResponse>>

}