package com.whiterabbit

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.example.androidpos.common.CircleTransform
import com.squareup.picasso.Picasso
import com.whiterabbit.models.EmployeeResponse
import com.whiterabbit.room.AppDatabase
import kotlinx.android.synthetic.main.activity_employe_details.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class EmployeDetailsActivity : AppCompatActivity() {
    private lateinit var instance: AppDatabase
    lateinit var employeeResponse:EmployeeResponse
    private val picasso = Picasso.get()
    lateinit var image:ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employe_details)
         image=findViewById<ImageView>(R.id.iv_profile_image)
        employeeResponse= EmployeeResponse()
        instance = AppDatabase.getInstance(this);
        supportActionBar!!.hide()
        val id=intent.getIntExtra("employeeid",0)
        GlobalScope.launch() {
            employeeResponse=instance!!.EmployeeDao().getEmployee(id.toString())
            runOnUiThread {
                    picasso.load(employeeResponse.profileImage).transform(CircleTransform()).into(image)
            }
            tv_name.text=employeeResponse.name
            tv_username.text=employeeResponse.username
            tv_address.text=employeeResponse.address!!.city
            tv_email.text=employeeResponse.email
            tv_companydetails.text=employeeResponse.company!!.catchPhrase
            tv_phone.text=employeeResponse.phone
            tv_website.text=employeeResponse.website
        }
    }}

